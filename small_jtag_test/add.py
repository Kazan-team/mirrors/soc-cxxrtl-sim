# generate add.il ilang file with: python3 add.py
#

from nmigen import Elaboratable, Signal, Module, Const, DomainRenamer
from nmigen.cli import verilog

# to get c4m-jtag
# clone with $ git clone gitolite3@git.libre-soc.org:c4m-jtag.git
#            $ git clone gitolite3@git.libre-soc.org:nmigen-soc.git
# for each:  $ python3 setup.py develop --user

from c4m.nmigen.jtag.tap import TAP, IOType


class ADD(Elaboratable):
    def __init__(self, width):
        self.a      = Signal(width)
        self.b      = Signal(width)
        self.f      = Signal(width)

        # set up JTAG
        self.jtag = TAP(ir_width=4)
        self.jtag.bus.tck.name = 'jtag_tck'
        self.jtag.bus.tms.name = 'jtag_tms'
        self.jtag.bus.tdo.name = 'jtag_tdo'
        self.jtag.bus.tdi.name = 'jtag_tdi'

        # have to create at least one shift register
        self.sr = self.jtag.add_shiftreg(ircode=4, length=3)

        # sigh and one iotype
        self.ios = self.jtag.add_io(name="test", iotype=IOType.In)

    def elaborate(self, platform):
        m = Module()

        m.submodules.jtag = jtag = self.jtag
        m.d.comb += self.sr.i.eq(self.sr.o) # loopback test

        # do a simple "add"
        m.d.sync += self.f.eq(self.a + self.b)
        m.d.sync += self.f[0].eq(Const(0, 1))

        return m


def create_verilog(dut, ports, test_name):
    vl = verilog.convert(dut, name=test_name, ports=ports)
    with open("%s.v" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    alu = DomainRenamer("sys")(ADD(width=4))
    create_verilog(alu, [alu.a, alu.b, alu.f,
                       alu.jtag.bus.tck,
                        alu.jtag.bus.tms,
                        alu.jtag.bus.tdo,
                        alu.jtag.bus.tdi], "add")
