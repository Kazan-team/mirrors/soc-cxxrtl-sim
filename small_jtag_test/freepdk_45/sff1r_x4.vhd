--
-- Generated by VASY
--
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY sff1r_x4 IS
PORT(
  ck	: IN BIT;
  i	: IN BIT;
  nrst	: IN BIT;
  q	: OUT BIT;
  vdd	: IN BIT;
  vss	: IN BIT
);
END sff1r_x4;

ARCHITECTURE RTL OF sff1r_x4 IS
  SIGNAL sff_m	: BIT;
BEGIN
  q <= sff_m;
  PROCESS ( ck, nrst )
  BEGIN
    IF  ((ck = '1') AND ck'EVENT)
    THEN 
        IF (nrst = '1')
        THEN sff_m <= i;
        END IF;
    END IF;
    IF  ((nrst = '0') AND nrst'EVENT)
    THEN sff_m <= '0';
    END IF;
  END PROCESS;
END RTL;
